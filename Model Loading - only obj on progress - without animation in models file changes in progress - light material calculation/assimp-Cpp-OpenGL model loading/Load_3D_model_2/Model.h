#pragma once

#include "GL\glew.h"
#include "Mesh.h"
#include <vector>
#include <string>
#include <map>
#include "glm\gtc\quaternion.hpp"


#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"



using namespace std;

class Model
{
public:
	Model();
	~Model();

	public:
		void loadModel(const string& path);
		void draw(GLuint shaders_program);


		Assimp::Importer import;

	const aiScene* scene;
	vector<Mesh> meshes; // one mesh in one object
	string directory;

	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	vector<Texture> LoadMaterialTexture(aiMaterial* mat, aiTextureType type, string type_name);
};

