#include "Model.h"
#include<Windows.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Texture.h"

#include <stdio.h>										// Standard I/O header 
#include <gl\gl.h>	
#pragma comment(lib, "Opengl32.lib")					//Link to OpenGL32.lib so we can use OpenGL stuff

	
FILE *gpmodelFile = NULL;
FILE * gpmodelFilechecker = NULL;

GLint TextureFromFile(const char *path)
{
	//Generate texture ID and load texture data
	string filename = string(path);

	GLuint textureID;
	glGenTextures(1, &textureID);


	int width, height, bpp;

	unsigned char* image = stbi_load(filename.c_str(), &width, &height, &bpp, 3);

	// rgb is now three bytes per pixel, width*height size. Or NULL if load failed.
	// Do something with it...



	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);


	stbi_image_free(image);

	return textureID;
}



GLint LoadGLTextures(const char *path)
{
	bool LoadTGA(LoadTexture *, char *);

	fprintf(gpmodelFile, "Entering LoadGLTextures\n");

	int Status = FALSE;										
	
	// Status Indicator
	// Load The Bitmap, Check For Errors.


	fprintf(gpmodelFile, "Entering LoadGLTextures %s\n",path);

	GLint textureid;

//	textureid = TextureFromFile(path);
		
//	return textureid;
	


		//tga
	
		LoadTexture texture;

		string filename = string((char *)path);

	
		if (LoadTGA(&texture, (char *)filename.c_str()))
		{
			Status = TRUE;											// Set The Status To TRUE

//			 Typical Texture Generation Using Data From The TGA ( CHANGE )
			glGenTextures(1, &texture.texID);				// Create The Texture ( CHANGE )
			glBindTexture(GL_TEXTURE_2D, texture.texID);
			glTexImage2D(GL_TEXTURE_2D, 0, texture.bpp / 8, texture.width, texture.height, 0, texture.type, GL_UNSIGNED_BYTE, texture.imageData);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			if (texture.imageData)						// If Texture Image Exists ( CHANGE )
			{
				free(texture.imageData);					// Free The Texture Image Memory ( CHANGE )
			}
		}
		textureid = texture.texID;
		return textureid;
	

	fprintf(gpmodelFile, "Leaving LoadGLTextures\n");
}



Model::Model()
{
	scene = nullptr;

	if (fopen_s(&gpmodelFile, "ModelLog.txt", "w") != 0)
	{
		exit(0);
	}
	else
	{
		fprintf(gpmodelFile, "Log File Is Successfully Opened.\n");
	}

	if (fopen_s(&gpmodelFilechecker, "gpmodelFilechecker.txt", "w") != 0)
	{
		exit(0);
	}
	else
	{
		fprintf(gpmodelFilechecker, "Log File Is Successfully Opened.\n");
	}

}


Model::~Model()
{

	import.FreeScene();
	fclose(gpmodelFile);
	fclose(gpmodelFilechecker);
}



void Model::draw(GLuint shaders_program)
{

	for (int i = 0; i < meshes.size(); i++)
	{
		meshes[i].Draw(shaders_program);
	}
}


void Model::loadModel(const string& path)
{
	// how work skeletal animation in assimp //translated with google =) :
	// node is a separate part of the loaded model (the model is not only a character)
	// for example, the camera, armature, cube, light source, part of the character's body (leg, rug, head).
	// a bone can be attached to the node
	// in the bone there is an array of vertices on which the bone affects (weights from 0 to 1).
	// each mChannels is one aiNodeAnim.
	// In aiNodeAnim accumulated transformations (scaling rotate translate) for the bone with which they have the common name
	// these transformations will change those vertices whose IDs are in the bone with a force equal to the weight.
	// the bone simply contains the ID and the weight of the vertices to which the transformation from aiNodeAnim is moving (with no common name for the bone)
	// (the vertex array and the weight of the transforms for each vertex are in each bone)

	// result: a specific transformation will affect a particular vertex with a certain force.

	scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		cout << "error assimp : " << import.GetErrorString() << endl;
		fprintf(gpmodelFilechecker, "%s\n", import.GetErrorString());
		return;
	}

	// directoru = container for model.obj and textures and other files
	directory = path.substr(0, path.find_last_of('/'));

	cout << "		name bones : " << endl;
	processNode(scene->mRootNode, scene);


}



void Model::processNode(aiNode* node, const aiScene* scene)
{
	Mesh mesh;
	for (uint i = 0; i < scene->mNumMeshes; i++)
	{
		aiMesh* ai_mesh = scene->mMeshes[i];
		mesh = processMesh(ai_mesh, scene);
		meshes.push_back(mesh); //accumulate all meshes in one vector
	}

}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{

	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;


	//size � resize - 
	//capacity � reserve - 
	//size - 
	//resize -
	//capacity -
	//reserve -

	vertices.reserve(mesh->mNumVertices); 
	indices.reserve(mesh->mNumVertices); 

	//vertices
	for (uint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;



		if (mesh->mNormals != NULL)
		{
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.normal = vector;
		}
		else
		{
			vertex.normal = glm::vec3();
		}
 

		// in assimp model can have 8 different texture coordinates
		// we only care about the first set of texture coordinates
		if (mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.text_coords = vec;

			//fprintf(gpmodelFile, "t\t :%f \t%f \t\n", vec.x, vec.y);

		}
		else
		{
			vertex.text_coords = glm::vec2(0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}

	//indices
	for (uint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i]; 
		indices.push_back(face.mIndices[0]); 
		indices.push_back(face.mIndices[1]); 
		indices.push_back(face.mIndices[2]);
	}

	//material
	if (mesh->mMaterialIndex >= 0)
	{
		//all pointers created in assimp will be deleted automaticaly when we call import.FreeScene();
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		vector<Texture> diffuse_maps = LoadMaterialTexture(material, aiTextureType_DIFFUSE, "texture_diffuse");
		bool exist = false;
		for (int i = 0; (i < textures.size()) && (diffuse_maps.size() != 0); i++)
		{
			if (textures[i].path ==  diffuse_maps[0].path) 
			{
				exist = true;
			}
		}
		if(!exist && diffuse_maps.size() != 0) textures.push_back(diffuse_maps[0]); 
		//textures.insert(textures.end(), diffuse_maps.begin(), diffuse_maps.end());

		vector<Texture> specular_maps = LoadMaterialTexture(material, aiTextureType_SPECULAR, "texture_specular");
		exist = false;
		for (int i = 0; (i < textures.size()) && (specular_maps.size() != 0); i++)
		{
			if (textures[i].path == specular_maps[0].path)
			{
				exist = true;
			}
		}
		if (!exist  && specular_maps.size() != 0) textures.push_back(specular_maps[0]); 
		//textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());

	}

	return Mesh(vertices, indices, textures);
}

vector<Texture> Model::LoadMaterialTexture(aiMaterial* mat, aiTextureType type, string type_name)
{
	vector<Texture> textures;
	for (uint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString ai_str;
		mat->GetTexture(type, i, &ai_str);

		string filename = string(ai_str.C_Str());
		filename = directory + '/' + filename;

		//cout << filename << endl;

		Texture texture;
		texture.id = LoadGLTextures(filename.c_str()); // return prepaired openGL texture
		texture.type = type_name;
		texture.path = ai_str;
		textures.push_back(texture);
	}
	return textures;
}














