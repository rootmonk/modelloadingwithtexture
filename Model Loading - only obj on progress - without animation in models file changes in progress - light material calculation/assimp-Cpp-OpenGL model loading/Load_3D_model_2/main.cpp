#include <windows.h>
#include <stdio.h> // for FILE I/O

#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>

#include <gl/GL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Model.h"


#include "Camera.h"
#include "Timer.h"


#pragma comment(lib,"glew32d.lib")
#pragma comment(lib,"opengl32.lib")

#pragma comment(lib,"SOIL.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
TCHAR *szCaption = (TCHAR *)TEXT("Shadow Orage Book");

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;


GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;

bool gbFirstMouse = true;
float gfLastX;
float gfLastY;
glm::mat4 projection;

GLfloat X = 0.0f, Y = 0.0f, Z = 0.0f;

Model ourModel;
CCamera camera;
Timer timer;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void spin(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	//Parallel to glutInitWindowSize(), glutInitWindowPosition() and glutCreateWindow() all three together
	hwnd = CreateWindow(szClassName,
		TEXT("OpenGL Programmable Pipeline Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//initialize
	initialize();
	timer.InitializeTimer();

	//Message Loop
	while (bDone == false) //Parallel to glutMainLoop();
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			timer.SetDeltaTime();
			timer.CalculateFPS();
			// rendring function
			display();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//variable declarations
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	int mouseX;
	int mouseY;
	DWORD flags;

	GLfloat fXOffset;
	GLfloat fYOffset;

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, the window is active
			gbActiveWindow = true;
		else //if non-zero, the window is not active
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE: //Parallel to glutReshapeFunc();
		resize(LOWORD(lParam), HIWORD(lParam)); //Parallel to glutReshapeFunc(resize);
		break;
	case WM_KEYDOWN: //Parallel to glutKeyboardFunc();
		switch (wParam)
		{
		case VK_ESCAPE: //case 27
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true; //Parallel to glutLeaveMainLoop();
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x41: // for 'A' or 'a'
			break;
		case 0x4C: // for 'L' or 'l'
			break;
		case VK_UP:
			fprintf(gpFile, "VK_UP Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_FORWARD, timer.GetDeltaTime());
			break;

		case VK_DOWN:
			fprintf(gpFile, "VK_DOWN Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_BACKWARD, timer.GetDeltaTime());
			break;

		case VK_LEFT:
			fprintf(gpFile, "VK_LEFT Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_LEFT, timer.GetDeltaTime());
			break;

		case VK_RIGHT:
			fprintf(gpFile, "VK_RIGHT Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_RIGHT, timer.GetDeltaTime());
			break;

		case VK_PRIOR:
			fprintf(gpFile, "VK_PRIOR Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_UP, timer.GetDeltaTime());
			break;

		case VK_NEXT:
			fprintf(gpFile, "VK_NEXT Key.\n");
			camera.ProcessNavigationKeys(CameraMovement::CAMERA_DOWN, timer.GetDeltaTime());
			break;
		default:
			break;
		}
		break;

	case WM_MOUSEMOVE:
		mouseX = LOWORD(lParam);
		mouseY = HIWORD(lParam);
		flags = (DWORD)wParam;
		if (gbFirstMouse)
		{
			gfLastX = (GLfloat)mouseX;
			gfLastY = (GLfloat)mouseY;
			gbFirstMouse = GL_FALSE;
		}

		fXOffset = mouseX - gfLastX;
		fYOffset = gfLastY - mouseY;

		gfLastX = (GLfloat)mouseX;
		gfLastY = (GLfloat)mouseY;

		if (flags & MK_LBUTTON)
		{
			camera.ProcessMouseMovement(fXOffset, fYOffset, GL_TRUE);
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'X':
			X -= 0.2f;
			break;
		case 'x':
			X += 0.2f;
			break;

		case 'Y':
			Y -= 0.2f;
			break;
		case 'y':
			Y += 0.2f;
			break;

		case 'Z':
			Z -= 0.2f;
			break;
		case 'z':
			Z += 0.2f;
			break;

		}
		break;

	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE: //Parallel to glutCloseFunc();
		uninitialize(); //Parallel to glutCloseFunc(uninitialize);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}


const GLchar* ReadShader(const char* filename)
{
	FILE *inputFile;						// Create a file pointer for the input shader file read
	fopen_s(&inputFile, filename, "rb");	// Open the file in secured mode. rb - for reading file other than plain text

	if (!inputFile)							// Check if file is opened
	{
		fprintf(gpFile, "Unable to open file : %s\n", filename);
		return(NULL);
	}

	// fseek(FILE *stream, long offset, int whence);
	fseek(inputFile, 0, SEEK_END);			// Set offset of 0 from the end of the file.	
	int len = ftell(inputFile);				// Obtain the position at the end of the file
	fprintf(gpFile, "The end of file is at : %d.\n", len);
	fseek(inputFile, 0, SEEK_SET);			// Reset to start once the end position is obtained

											// Create a GLchar array and allocate memory for it equal to length plus one:
	GLchar* shaderSourceCode = new GLchar[len + 1];
	// fread(void *ptr, size_t size, size_t count, FILE* stream);
	// Read an array of 'count' elements, each one with a size of 'size' bytes (here 1 for char), from 'stream' and store
	// in a block of memory specified by 'ptr':
	fread(shaderSourceCode, 1, len, inputFile);
	fclose(inputFile);						// Close the file

	shaderSourceCode[len] = 0;				// Append '0' at the end of the string for NULL termination

	return(shaderSourceCode);				// Return the string
}


//FUNCTION DEFINITIONS
void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	//Parallel to glutInitDisplayMode()
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	//choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	//make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW Initialization Code For GLSL ( IMPORTANT : It Must Be Here. Means After Creating OpenGL Context But Before Using Any OpenGL Function )
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	//const GLchar *vertexShaderSourceCode = ReadShader("modelLoading.vert");

	const GLchar *vertexShaderSourceCode =
		"#version 430" \
		"\n" \
		"layout ( location = 0 ) in vec4 vPosition;" \
		"layout(location = 1) in vec3 vNormal; "\
		"layout ( location = 2 ) in vec2 texCoords;" \
		"out vec2 text_coords;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
			"vec4 eye_coordinates =u_view_matrix * u_model_matrix * vPosition;" \

			"\n" \

			"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \

		"\n" \

			"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \

			"text_coords = texCoords;" \

		"\n" \

			"viewer_vector = -eye_coordinates.xyz;" \

		"\n" \

			"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
		const GLchar *fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec2 text_coords;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \

			"\n" \

		"struct Material" \
		"{" \
			
			"sampler2D texture_diffuse1;" \

			"sampler2D texture_specular1; " \

			"float shininess;" \
			
			"float transparency; " \
		
		"};"  \

			"\n" \

		"struct PointLight " \
		"{" \
			
			"vec3 position;" \

			"vec3 ambient;" \

			"vec3 diffuse; " \
			
			"vec3 specular; " \

			"float constant; " \
			
			"float linear;"  \

			"float quadratic; " \
		
		"};" \

			"\n" \

		"uniform Material material;" \
		"uniform PointLight light;" \

			"\n" \

			"void main(void)" \
			"{" \
						"vec3 phong_ads_color;" \
						"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
						"vec3 normalized_light_direction=normalize(light_direction);" \
						"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		
					"\n" \

						"vec3 texture_ambient = vec3(texture(material.texture_diffuse1, text_coords));" \

					"\n" \


//					"vec3 ambient = light.ambient  * texture_ambient ;" \

					"vec3 ambient =  texture_ambient ;" \


					"\n" \

					"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \

					"\n" \



//					"vec3 diffuse = light.diffuse * vec3(texture(material.texture_diffuse1, text_coords)) * tn_dot_ld;" \



					"vec3 diffuse =  vec3(texture(material.texture_diffuse1, text_coords)) ;" \



					"\n" \

					"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
			
					"\n" \


//					"vec3 specular = light.specular  * vec3(texture(material.texture_specular1, text_coords)) * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), material.shininess);" \

					"vec3 specular =  vec3(texture(material.texture_specular1, text_coords));" \


					"\n" \

					"phong_ads_color=ambient + diffuse + specular;" \

					"phong_ads_color*=material.transparency;" \

					"\n" \

					"FragColor = vec4(phong_ads_color, 1.0);" \



					"\n" \

			"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}





	


	

	

	//Final Working
	//ourModel.loadModel("models/BroadleafOBJ/Broadleaf_Desktop_Field.obj");

	//tree with texture working
	
//	ourModel.loadModel("models/mytrees/2632.obj");

	//treehouse with texture working

	//ourModel.loadModel("models/mytrees/2632.obj");


	//ourModel.loadModel("models/Tree-3d-free/Tree-3d-free.fbx");


	ourModel.loadModel("models/OC12_Eucalyptus_globulus_Bluegum/OC12_1.obj");
	

	glEnable(GL_BLEND);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f); // black

	// set perspective matrix to identitu matrix

	projection = glm::mat4();
	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object


	glUseProgram(gShaderProgramObject);

	glm::mat4 view = glm::mat4(1.0f);
	
	view = camera.GetViewMatrix();

	glUniformMatrix4fv(glGetUniformLocation(gShaderProgramObject, "u_projection_matrix"), 1, GL_FALSE, glm::value_ptr(projection));

	glUniformMatrix4fv(glGetUniformLocation(gShaderProgramObject, "u_view_matrix"), 1, GL_FALSE, glm::value_ptr(view));

	// Draw the loaded model

	glm::mat4 model;

	model = glm::translate(model, glm::vec3(0.0f, 0.0f, -5.0f)); // Translate it down a bit so it's at the center of the scene

	glUniformMatrix4fv(glGetUniformLocation(gShaderProgramObject, "u_model_matrix"), 1, GL_FALSE, glm::value_ptr(model));

	glUniform3f(glGetUniformLocation(gShaderProgramObject, "u_light_position"), 0.0f, 0.0f, -1500.f);


	glUniform1f(glGetUniformLocation(gShaderProgramObject, "material.shininess"), 32.0f);
	glUniform1f(glGetUniformLocation(gShaderProgramObject, "material.transparency"), 1.0f);

	// Point Light 1

	glUniform3f(glGetUniformLocation(gShaderProgramObject, "point_light.ambient"), 0.1f, 0.1f, 0.1f);
	glUniform3f(glGetUniformLocation(gShaderProgramObject, "point_light.diffuse"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(gShaderProgramObject, "point_light.specular"), 1.0f, 1.0f, 1.0f);

	glUniform1f(glGetUniformLocation(gShaderProgramObject, "point_light.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(gShaderProgramObject, "point_light.linear"), 0.007);
	glUniform1f(glGetUniformLocation(gShaderProgramObject, "point_light.quadratic"), 0.0002);

	ourModel.draw(gShaderProgramObject);

	glUseProgram(gShaderProgramObject);


	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	projection = glm::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 10000.0f);
}


void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}


	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment  shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	//Deselect the rendering context
	wglMakeCurrent(NULL, NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
