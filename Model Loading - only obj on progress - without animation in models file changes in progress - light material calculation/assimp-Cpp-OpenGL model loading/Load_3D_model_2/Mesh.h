#pragma once

#include <iostream>
#include "GL\glew.h"
#include "glm\glm.hpp"
#include "assimp\Importer.hpp"
#include <vector>
#include <string>


using namespace std;
typedef unsigned int uint;

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 text_coords;
};

struct Texture
{
	GLuint id;
	string type;
	aiString path;
};




class Mesh
{
public:
	Mesh(vector<Vertex> vertic, vector<GLuint> ind, vector<Texture> textur);
	Mesh() {};
	~Mesh();

	// Render mesh
	void Draw(GLuint shaders_program);

private:
	//Mesh data
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;

	//buffers
	GLuint VAO;
	GLuint VBO_vertices;
	GLuint EBO_indices;

	//inititalize buffers
	void SetupMesh();
};
