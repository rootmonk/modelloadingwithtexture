#include "Timer.h"

extern HWND ghwnd;
extern TCHAR *szCaption;

void Timer::InitializeTimer(void)
{
	// counts per second
	QueryPerformanceFrequency((LARGE_INTEGER *)&iCountPerSecond);

	// seconds per count
	fSecondsPerCount = 1.0f / iCountPerSecond;

	// previous time
	QueryPerformanceCounter((LARGE_INTEGER *)&iPreviousTime);
}

void Timer::SetDeltaTime(void)
{
	// get current count
	QueryPerformanceCounter((LARGE_INTEGER *)&iCurrentTime);

	// delta time
	fDeltaTime = (iCurrentTime - iPreviousTime) * fSecondsPerCount;
	fElapsedTime += fDeltaTime;
}

float Timer::GetDeltaTime(void)
{
	return fDeltaTime;
}

void Timer::CalculateFPS(void)
{
	iFrameCount++;
	TCHAR str[255];

	if (fElapsedTime >= 1.0f)
	{
		fps__ = iFrameCount;

		swprintf_s(str, 255, TEXT("%s FPS: %d"), szCaption, fps__);
		SetWindowText(ghwnd, str);

		iFrameCount = 0;
		fElapsedTime = 0.0f;
	}

	iPreviousTime = iCurrentTime;
}